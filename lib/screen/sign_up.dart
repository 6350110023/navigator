

import 'package:flutter/material.dart';
import 'package:navigatortest/model/profile_signup.dart';
import 'package:navigatortest/screen/sign_in.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}
class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController email = new TextEditingController();
  TextEditingController name = new TextEditingController();
  TextEditingController password = new TextEditingController();

  final fromKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20,50,20 , 0),
            child: Column(
              children: [
                CircleAvatar(backgroundColor: Colors.black, radius: 120,
                  child: Icon(Icons.account_box,size: 150,color: Colors.white70,),),
                Text('Create Account',style: TextStyle(
                    fontSize:40,
                    fontWeight: FontWeight.w500,
                    color: Colors.black)),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: TextField(
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.black
                    ),
                    controller: name,
                    decoration: InputDecoration(
                        hintText: 'Username',
                        prefixIcon: Icon(
                          Icons.person,
                          color: Colors.black,
                        ),
                        hintStyle: TextStyle(
                            fontSize: 20,
                            color: Colors.black
                        )
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: TextField(
                    obscureText: true,
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.black
                    ),
                    controller: password,
                    decoration: InputDecoration(
                        hintText: 'Password',
                        prefixIcon: Icon(
                          Icons.lock_person,
                          color: Colors.black,
                        ),
                        hintStyle: TextStyle(
                            fontSize: 20,
                            color: Colors.black
                        )
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: TextField(
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.black
                    ),
                    controller: email,
                    decoration: InputDecoration(
                        hintText: 'Email',
                        prefixIcon: Icon(
                          Icons.email_rounded,
                          color: Colors.black,
                        ),
                        hintStyle: TextStyle(
                            fontSize: 20,
                            color: Colors.black
                        )
                    ),
                  ),
                ),
                ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.red),

                  ),
                  child: Text("Register",style: TextStyle(fontSize: 20),),
                  onPressed: () async {
                    Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => Profile_Signup(
                                name: name.text,
                                email: email.text,
                                password: password.text)));
                  },
                ),
              ],
            ),
          ),
        ),
    );
  }
}
