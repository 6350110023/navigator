


import 'package:flutter/material.dart';
import 'package:navigatortest/screen/sign_in.dart';
import 'package:navigatortest/screen/sign_up.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 120, 10, 0),
        child: Column(
          children: [

            Image.asset('assets/3.png'),
            SizedBox(
              width: 300, height: 50,
              child:ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.black),
                    ),

                child: Text("Sign up",style: TextStyle(fontSize: 25),),
                onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return RegisterScreen();
                    })
                    );
                },
              ),

            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                width: 300, height: 50,
                child:ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.red),
                  ),
                  child: Text("Sign in",style: TextStyle(fontSize: 25),),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return LoginScreen();
                    })
                    );
                  },

                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
