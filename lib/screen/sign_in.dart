

import 'package:flutter/material.dart';
import 'package:navigatortest/model/profile.dart';
import 'package:navigatortest/screen/sign_up.dart';


class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}


class _LoginScreenState extends State<LoginScreen> {
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8,50, 8, 0),
          child: Column(
            children: [
              CircleAvatar(backgroundColor: Colors.black, radius: 120,
                child: Icon(Icons.person,size: 150,color: Colors.white70,),),
              Text('Sign in',style: TextStyle(
                  fontSize:50,
                  fontWeight: FontWeight.w900,
                  color: Colors.black87)),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: TextField(
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black
                  ),
                  controller: email,
                  decoration: InputDecoration(
                      hintText: 'Username',
                      prefixIcon: Icon(
                        Icons.person_pin,
                        color: Colors.black,
                      ),
                      hintStyle: TextStyle(
                          fontSize: 20,
                          color: Colors.black
                      )
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: TextField(
                  obscureText: true,
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.black
                  ),
                  controller: password,
                  decoration: InputDecoration(
                      hintText: 'Password',
                      prefixIcon: Icon(
                        Icons.lock_person,
                        color: Colors.black,
                      ),
                      hintStyle: TextStyle(
                          fontSize: 20,
                          color: Colors.black
                      )
                  ),
                ),
              ),
              ElevatedButton(
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.red),

                ),
                child: Text("Sign in",style: TextStyle(fontSize: 25),),
                onPressed: () async {
                  Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => Profile_login(
                              email: email.text,
                              password: password.text)));
                },
              ),
              GestureDetector(
                onTap: (){Navigator.push(context, MaterialPageRoute(
                    builder: (context){
                      return RegisterScreen();
                    })
                );
                },
                child: Text('Register',style: TextStyle(fontSize: 18,
                    fontWeight: FontWeight.bold,color: Colors.grey),),
              )
            ],
          ),
        ),
      ),
    );
  }
}
