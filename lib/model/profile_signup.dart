


import 'package:flutter/material.dart';

class Profile_Signup extends StatelessWidget {
  String name,email,password;

  Profile_Signup(
      {Key? key, required this.name,required this.email, required this.password})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(40, 150, 0, 0),
          child: Column(
            children: [
              CircleAvatar(backgroundColor: Colors.black, radius: 150,
                child: Icon(Icons.person,size: 200,color: Colors.red,),),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 45, 0),
                child: Text('Welcome My user',style: TextStyle(fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Name:  $name",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Email:  $email",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Password:  $password",
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
