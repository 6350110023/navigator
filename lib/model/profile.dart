


import 'package:flutter/material.dart';

class Profile_login extends StatelessWidget {
  String email,password;

   Profile_login(
       {Key? key, required this.email, required this.password})
       : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(40, 150, 0, 0),
            child: Column(
              children: [
                CircleAvatar(backgroundColor: Colors.black87, radius: 150,
                  child: Icon(Icons.account_box,size: 200,color: Colors.white70,),),
                Padding(
                  padding: const EdgeInsets.fromLTRB(20, 10, 45, 0),
                  child: Text('Welcome My user',style: TextStyle(fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                      "Email:  $email",
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
              ),
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: Text(
                    "Password:  $password",
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),

              ],
            ),
          ),
      ),
    );
  }
}
